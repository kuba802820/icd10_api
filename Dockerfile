FROM node:lts-alpine3.17

COPY ["package.json", "package-lock.json", "tsconfig.json", "./"]
COPY ["./src", "./src"]
RUN npm install
RUN npm run build


CMD ["node", "./dist/server.js"]

EXPOSE 7650
