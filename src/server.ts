import cors from "@fastify/cors";
import helmet from "@fastify/helmet";
import * as dotenv from "dotenv";
import fastify from "fastify";
import pg from "pg";
import format from "pg-format";
import querystring from "querystring";
import { Req } from "utils/types";
import { validateKey } from "./middlewares/validateParams";
import { RESPONSES_MESSAGES } from "./utils/messages";
dotenv.config();

const app = fastify({
  logger: process.env.MODE === "development",
  querystringParser: (str) => querystring.parse(str.toLowerCase()),
  ignoreTrailingSlash: true,
});

app.register(helmet);
app.register(cors, {
  origin: ["https://sgr-med.web.app", "http://localhost:3000"],
  methods: ["GET"],
  allowedHeaders: ["Content-Type", "x-api-key"],
});

const pool = new pg.Pool({
  connectionString: process.env.DATABASE_URL,
});

const getOrigins = async () => {
  const origins: string[] = [];
  const query = await pool.query<{ assigned_for: string }>(
    format("SELECT assigned_for FROM icd10_api_keys")
  );
  query.rows.map(({ assigned_for }) => {
    origins.push(assigned_for);
  });
  return origins;
};

app.addHook("preHandler", validateKey);
if (process.env.MODE !== "development") {
  app.addHook("onRequest", async (req: Req, res) => {
    const apiKey = req.headers["x-api-key"] as string;
    const clientOrigin = req.headers["origin"] as string;
    try {
      const result = await pool.query(
        format("SELECT * FROM icd10_api_keys WHERE key=$1 AND assigned_for=$2"),
        [apiKey, clientOrigin]
      );
      if (result.rowCount === 0) {
        res.code(403).send({
          error: RESPONSES_MESSAGES.WRONG_API_KEY,
        });
      }
    } catch (error) {
      console.error(error);
      res.code(500).send({
        error: RESPONSES_MESSAGES.INTERNAL_SERVER_ERROR,
      });
    }
  });
}

app.register(import("@fastify/rate-limit"), {
  max: 30,
  timeWindow: "1 minute",
});

app.setErrorHandler(function (error, _request, reply) {
  if (reply.statusCode === 429) {
    error.message = RESPONSES_MESSAGES.RATE_LIMIT_EXCEEDED;
  }
  reply.send(error);
});
app.register(
  (instance, _opts, next) => {
    instance.get("/status", (req) => {
      const clientOrigin = req.headers["origin"] as string;
      console.log(`ORIGIN: ${clientOrigin}`);
      return { status: "ok" };
    });
    instance.get("/search_by_description", async (req: Req, res) => {
      const page = req.query.page || 1;
      const limit = 15;
      const offset = (page - 1) * limit;

      const countQuery = await pool.query(
        format(
          "SELECT COUNT(*) AS count FROM icd10_codes WHERE content LIKE $1"
        ),
        [`%${req.query.key}%`]
      );

      const totalCount = parseInt(countQuery.rows[0].count);
      const { rows } = await pool.query(
        format(
          "SELECT * FROM icd10_codes INNER JOIN icd10_chapters i10c on icd10_codes.chapter_id = i10c.id WHERE LOWER(content) ILIKE $1 LIMIT $2 OFFSET $3"
        ),
        [`%${req.query.key}%`, limit, offset]
      );

      const totalPages = Math.ceil(totalCount / limit);

      const results = {
        total_count: totalCount,
        total_pages: totalPages,
        current_page: page,
        results: rows,
      };
      if (rows.length > 0) {
        return res.send(results);
      } else {
        return res.status(422).send({ error: RESPONSES_MESSAGES.NOT_FOUND });
      }
    });

    instance.get("/search_by_icd", async (req: Req, res) => {
      const { rows } = await pool.query(
        format("SELECT * FROM icd10_codes WHERE code = $1"),
        [req.query.key.toUpperCase()]
      );
      return rows.length > 0
        ? rows
        : res.status(422).send({ error: RESPONSES_MESSAGES.NOT_FOUND });
    });

    next();
  },
  { prefix: "api" }
);

const main = async () => {
  try {
    getOrigins();
    await app.listen({
      port: +(process.env.PORT || false) || 8000,
      host: "0.0.0.0",
    });
  } catch (error) {
    console.error(error);
    process.exit(1);
  }
};

main();
