import { RESPONSES_MESSAGES } from "../utils/messages";
import { Req } from "../utils/types";

export const validateKey = (req: Req, res: any, next: any) => {
  const ignoresPath = ["/api/status"];

  if (
    !ignoresPath.includes(req.routerPath) &&
    (!req.query.key ||
      typeof req.query.key !== "string" ||
      req.query.key.length < 3 ||
      req.query.key.length > 100 ||
      req.query.page <= 0)
  ) {
    res.code(422).send({
      error: RESPONSES_MESSAGES.WRONG_PARAM,
    });
  } else {
    next();
  }
};
