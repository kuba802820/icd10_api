export const RESPONSES_MESSAGES = {
  NOT_FOUND: "Nic nie znaleziono!",
  WRONG_API_KEY: "Klucz API jest niepoprawny!",
  WRONG_PARAM: "Podany parametr jest błędny!",
  RATE_LIMIT_EXCEEDED: "Przekroczony został limit zapytań!",
  INTERNAL_SERVER_ERROR: "Wystąpił wewnętrzny błąd serwera!",
  ACCESS_DENIED: "Brak dostępu!",
};
