import { FastifyRequest } from "fastify";

export type Req = FastifyRequest<{
  Querystring: { key: string; page: number };
}>;
